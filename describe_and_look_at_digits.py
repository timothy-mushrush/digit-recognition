# Import necessary modules
from sklearn import datasets
import matplotlib.pyplot as plt


# Load the digits dataset: digits
digits = datasets.load_digits()


def look_at_digits():
    """
    Print the keys and DESCR of the dataset
    Print the shape of the images and data keys
    Display digit 1010
    """
    # Print the keys and DESCR of the dataset
    print(digits.keys())
    print(digits.DESCR)

    # Print the shape of the images and data keys
    print(digits.images.shape)
    print(digits.data.shape)

    digit = int(input("What digit do you want to see; (0-1797)?"))

    # Display digit
    plt.imshow(digits.images[digit], cmap=plt.cm.gray_r, interpolation='nearest')
    plt.show()
    return None


def main():
    """Run the program.

    Returns:
        digits: The digits dataset.
    """
    look = input("Do you want to look at the digits dataset? (y/n)")
    if look == 'y':
        look_at_digits()
    return digits


if __name__ == '__main__':
    main()
